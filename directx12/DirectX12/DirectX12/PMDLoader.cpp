#include <memory>
#include <array>
#include <map>
#include "PMDLoader.h"
#include "pmdModel/PMDModel.h"

using namespace DirectX;

PMDLoader::PMDLoader()
{
}

std::string PMDLoader::GetExtension(const std::string& path)
{
	int index = path.rfind('.');
	return path.substr(index + 1, path.length() - index - 1);
}

std::string PMDLoader::GetTexturePathFromModelAndTexPath(const std::string& modelPath, const char* texpath)
{
	int pathIndex1 = modelPath.rfind('/');
	int pathIndex2 = modelPath.rfind('\\');
	auto pathIndex = max(pathIndex1, pathIndex2);
	auto folderPath = modelPath.substr(0, pathIndex + 1);

	return folderPath + texpath;
}

TextureResource PMDLoader::LoadTexture(std::shared_ptr<PMDModel>& pmdModel, std::string pmdPathStr, Microsoft::WRL::ComPtr<ID3D12Device>& dev)
{
	TextureResource texResource;
	texResource.resize(pmdModel->GetMaterial().size());

	auto LoadTex = [&](std::string& texStr)
	{
		auto texFilePath = GetTexturePathFromModelAndTexPath(pmdPathStr, texStr.c_str());
		if (GetExtension(texStr) == "sph")
		{
			return std::make_pair("sph", LoadTextureFromFile(texFilePath,dev));
		}
		else if (GetExtension(texStr) == "spa")
		{
			return std::make_pair("spa", LoadTextureFromFile(texFilePath, dev));
		}
		else
		{
			return std::make_pair("bmp", LoadTextureFromFile(texFilePath,dev));
		}

		throw;
		return std::make_pair("bmp", LoadTextureFromFile(texFilePath,dev));
	};

	auto whiteTex = CreateAnotherTexture(true,dev);
	auto blackTex = CreateAnotherTexture(false,dev);

	for (int i = 0; i < pmdModel->GetMaterial().size(); i++)
	{
		texResource[i]["sph"] = whiteTex;
		texResource[i]["spa"] = blackTex;
		texResource[i]["bmp"] = whiteTex;

		std::string strTex = pmdModel->GetMaterial()[i].texFilePath;

		if (strTex == "")
		{
			continue;
		}
		if (count(strTex.begin(), strTex.end(), '*') > 0)
		{
			std::array<std::string, 2> texArray;
			auto namepair = SplitFileName(strTex);
			texArray[0] = namepair.first;
			texArray[1] = namepair.second;
			for (auto& tex : texArray)
			{
				if (tex != "")
				{
					auto texpair = LoadTex(tex);
					texResource[i][texpair.first] = texpair.second;
				}
			}
		}
		else
		{
			auto texpair = LoadTex(strTex);
			texResource[i][texpair.first] = texpair.second;
		}
	}

	return texResource;
}

std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> PMDLoader::LoadToon(std::shared_ptr<PMDModel>& pmdModel, std::string texStr, Microsoft::WRL::ComPtr<ID3D12Device>& dev)
{
	std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> toonResources;
	toonResources.resize(pmdModel->GetMaterial().size());
	auto toonPaths = pmdModel->GetToonPath();
	for (int i = 0; i < pmdModel->GetMaterial().size(); i++)
	{
		// トゥーンリソースの読み込み
		if (toonPaths[i] != "")
		{
			auto strToonPath = GetTexturePathFromModelAndTexPath(texStr.c_str(), toonPaths[i].c_str());

			if (!CreateTexture(strToonPath, toonResources[i], dev))
			{
				strToonPath = "toon/" + toonPaths[i];
				CreateTexture(strToonPath, toonResources[i], dev);
			}
		}
	}

	return toonResources;
}


std::pair<std::string, std::string> PMDLoader::SplitFileName(const std::string& path, const char splitter)
{
	int index = path.find(splitter);
	std::pair <std::string, std::string> ret;
	ret.first = path.substr(0, index);
	ret.second = path.substr(index + 1, path.length() - index - 1);

	return ret;
}

bool PMDLoader::CreateTexture(std::string& texPath, Microsoft::WRL::ComPtr<ID3D12Resource>& toonRes, Microsoft::WRL::ComPtr<ID3D12Device>& dev)
{
	TexMetadata metadata = {};
	ScratchImage scratchImg = {};

	auto result = LoadFromWICFile(GetWideStringFromString(texPath).c_str(), WIC_FLAGS_NONE, &metadata, scratchImg);
	if (result != S_OK)
	{
		return false;
	}

	// WriteToSubresourceで転送する用のヒープ設定
	D3D12_HEAP_PROPERTIES texHeapProp = {};
	texHeapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
	texHeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
	texHeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
	texHeapProp.CreationNodeMask = 0;
	texHeapProp.VisibleNodeMask = 0;

	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = metadata.format;
	resDesc.Width = metadata.width;
	resDesc.Height = metadata.height;
	resDesc.DepthOrArraySize = metadata.arraySize;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.MipLevels = metadata.mipLevels;
	resDesc.Dimension = static_cast<D3D12_RESOURCE_DIMENSION>(metadata.dimension);
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	ID3D12Resource* texbuff = nullptr;
	result = dev->CreateCommittedResource(
		&texHeapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&texbuff));
	if (result != S_OK)
	{
		return false;
	}

	auto img = scratchImg.GetImage(0, 0, 0);
	result = texbuff->WriteToSubresource(
		0,
		nullptr,
		img->pixels,
		img->rowPitch,
		img->slicePitch);
	if (result != S_OK)
	{
		return false;
	}

	toonRes = texbuff;

	return true;
}

Microsoft::WRL::ComPtr<ID3D12Resource> PMDLoader::LoadTextureFromFile(std::string& texPath, Microsoft::WRL::ComPtr<ID3D12Device>& dev)
{
	TexMetadata metadata = {};
	ScratchImage scratchImg = {};

	auto it = resourceTbl_.find(texPath);

	HRESULT result = S_OK;

	if (it != resourceTbl_.end())
	{
		return resourceTbl_[texPath];
	}

	if (GetExtension(texPath) == "tga")
	{
		result = LoadFromTGAFile(GetWideStringFromString(texPath).c_str(),
			&metadata,
			scratchImg);
		assert(SUCCEEDED(result));
	}
	else
	{
		result = LoadFromWICFile(GetWideStringFromString(texPath).c_str(),
			WIC_FLAGS_NONE,
			&metadata,
			scratchImg);
		assert(SUCCEEDED(result));
	}

	auto img = scratchImg.GetImage(0, 0, 0);

	// WriteToSubresourceで転送する用のヒープ設定
	D3D12_HEAP_PROPERTIES texHeapProp = {};
	texHeapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
	texHeapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
	texHeapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
	texHeapProp.CreationNodeMask = 0;
	texHeapProp.VisibleNodeMask = 0;
	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = metadata.format;
	resDesc.Width = metadata.width;
	resDesc.Height = metadata.height;
	resDesc.DepthOrArraySize = metadata.arraySize;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.MipLevels = metadata.mipLevels;
	resDesc.Dimension = static_cast<D3D12_RESOURCE_DIMENSION>(metadata.dimension);
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	ID3D12Resource* texbuff = nullptr;
	result = dev->CreateCommittedResource(
		&texHeapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&texbuff));
	assert(SUCCEEDED(result));

	result = texbuff->WriteToSubresource(
		0,
		nullptr,
		img->pixels,
		img->rowPitch,
		img->slicePitch);
	assert(SUCCEEDED(result));

	resourceTbl_[texPath] = texbuff;

	return texbuff;
}

Microsoft::WRL::ComPtr<ID3D12Resource> PMDLoader::CreateAnotherTexture(bool whiteTexture, Microsoft::WRL::ComPtr<ID3D12Device>& dev)
{
	D3D12_RESOURCE_DESC resDesc = {};
	resDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	resDesc.Width = 4;
	resDesc.Height = 4;
	resDesc.DepthOrArraySize = 1;
	resDesc.SampleDesc.Count = 1;
	resDesc.SampleDesc.Quality = 0;
	resDesc.MipLevels = 1;
	resDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	D3D12_HEAP_PROPERTIES heapProp = {};
	heapProp.Type = D3D12_HEAP_TYPE_CUSTOM;
	heapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
	heapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
	heapProp.CreationNodeMask = 0;
	heapProp.VisibleNodeMask = 0;

	ID3D12Resource* texbuff = nullptr;
	auto result = dev->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&resDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		nullptr,
		IID_PPV_ARGS(&texbuff));
	assert(SUCCEEDED(result));

	std::vector<uint8_t> data(4 * 4 * 4);
	if (whiteTexture)
	{
		std::fill(data.begin(), data.end(), 0xff);
		result = texbuff->WriteToSubresource(
			0,
			nullptr,
			data.data(),
			4 * 4,
			data.size());
	}
	else
	{
		std::fill(data.begin(), data.end(), 0);
		result = texbuff->WriteToSubresource(
			0,
			nullptr,
			data.data(),
			4 * 4,
			data.size());
	}
	assert(SUCCEEDED(result));

	return texbuff;
}

std::wstring PMDLoader::GetWideStringFromString(const std::string& str)
{
	//呼び出し1回目(文字列数を得る)
	auto num1 = MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED | MB_ERR_INVALID_CHARS, str.c_str(), -1, nullptr, 0);

	std::wstring wstr;//stringのwchar_t版
	wstr.resize(num1);//得られた文字列数でリサイズ

	//呼び出し2回目(確保済みのwstrに変換文字列をコピー)
	auto num2 = MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED | MB_ERR_INVALID_CHARS, str.c_str(), -1, &wstr[0], num1);
	assert(num1 == num2);

	return wstr;
}