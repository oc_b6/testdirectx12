#pragma once
#include <Windows.h>
#include <d3d12.h>
#include <wrl.h>
#include <vector>
#include <string>
#include <map>
#include <DirectXTex.h>

#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"DirectXTex.lib")

using TexType = std::map<std::string, Microsoft::WRL::ComPtr<ID3D12Resource>>;
using TextureResource = std::vector <TexType>;

class PMDModel;
class PMDLoader
{
public:

	static PMDLoader& Instance()
	{
		static PMDLoader s_instance;
		return s_instance;
	}

	// ファイル名から拡張子を取得する
	std::string GetExtension(const std::string& path);
	
	std::string GetTexturePathFromModelAndTexPath(const std::string& modelPath, const char* texpath);
	
	std::wstring GetWideStringFromString(const std::string& str);
	
	TextureResource LoadTexture(std::shared_ptr<PMDModel>& pmdModel,std::string texStr, Microsoft::WRL::ComPtr<ID3D12Device>& dev);
	
	std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> LoadToon(std::shared_ptr<PMDModel>& pmdModel, std::string texStr, Microsoft::WRL::ComPtr<ID3D12Device>& dev);
	
	Microsoft::WRL::ComPtr<ID3D12Resource> LoadTextureFromFile(std::string& texPath, Microsoft::WRL::ComPtr<ID3D12Device>& dev);
private:
	PMDLoader();
	PMDLoader(const PMDLoader&) = delete;
	void operator=(const PMDLoader&) = delete;

	std::map<std::string, Microsoft::WRL::ComPtr<ID3D12Resource>> resourceTbl_;

	std::pair<std::string, std::string> SplitFileName(const std::string& path, const char splitter = '*');

	bool CreateTexture(std::string& texPath, Microsoft::WRL::ComPtr<ID3D12Resource>& toonRes, Microsoft::WRL::ComPtr<ID3D12Device>& dev);

	Microsoft::WRL::ComPtr<ID3D12Resource> CreateAnotherTexture(bool whiteTexture, Microsoft::WRL::ComPtr<ID3D12Device>& dev);
};