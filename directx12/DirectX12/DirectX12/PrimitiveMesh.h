#pragma once
#include <Windows.h>
#include <d3d12.h>
#include <wrl.h>
#include <DirectXMath.h>
#include <vector>

using Microsoft::WRL::ComPtr;

struct PrimitiveData
{
	PrimitiveData(DirectX::XMFLOAT3 p, DirectX::XMFLOAT3 n, DirectX::XMFLOAT2 u) :pos(p),normal_vec(n),uv(u) {}
	DirectX::XMFLOAT3 pos;	// 座標
	DirectX::XMFLOAT3 normal_vec;  // 法線ベクトル
	DirectX::XMFLOAT2 uv;	// UV
};

class PrimitiveMesh
{
public:
	PrimitiveMesh(ComPtr<ID3D12Device>& dev, ComPtr<ID3D12GraphicsCommandList>& cmdList,DirectX::XMFLOAT3 pos,float width,float depth) :
		dev_(dev),cmdList_(cmdList),pos_(pos),width_(width),depth_(depth) {}
	~PrimitiveMesh();

	void InitPrimRootSignature();

	void CreatePrimBufferAndView();

	void CreatePrimPipeline();

	void DrawPrim(ComPtr<ID3D12DescriptorHeap>& transformHeap, ComPtr<ID3D12DescriptorHeap>& shadowSRVHeap);

private:
	std::vector<PrimitiveData> vertices_;		// 頂点データ
	ComPtr<ID3D12Resource> vertexBuffer_ = nullptr;
	D3D12_VERTEX_BUFFER_VIEW vbView_;

	ComPtr<ID3D12PipelineState> primPipeline_ = nullptr;
	ComPtr<ID3D12RootSignature> primRootSignature_ = nullptr;

	D3D12_VIEWPORT primViewPort_;	// ビューポート
	D3D12_RECT primScissor_;

	ComPtr<ID3D12Device> dev_;
	ComPtr<ID3D12GraphicsCommandList> cmdList_ = nullptr;
	DirectX::XMFLOAT3 pos_;
	float width_;
	float depth_;
};

