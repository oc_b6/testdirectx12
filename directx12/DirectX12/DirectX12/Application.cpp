#include "Application.h"
#include <tchar.h>
#include <iostream>
#include "Directx12/Dx12Wrapper.h"

constexpr int WINDOW_WIDTH = 800;
constexpr int WINDOW_HEIGHT = 600;

using namespace std;

LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

void Application::Initialize()
{
	windowClass_ = {};
	windowClass_.cbSize = sizeof(WNDCLASSEX);
	windowClass_.lpfnWndProc = (WNDPROC)WindowProcedure;	// コールバック関数の指定
	windowClass_.lpszClassName = _T("DirectXTest"); //アプリケーションクラス名
	windowClass_.hInstance = GetModuleHandle(0); // ハンドルの取得
	RegisterClassEx(&windowClass_);

	RECT wrc = { 0,0,WINDOW_WIDTH,WINDOW_HEIGHT }; // ウィンドウサイズを決める
	AdjustWindowRect(&wrc, WS_OVERLAPPEDWINDOW, false); // ウィンドウサイズを関数を使って補正する

	hwnd_ = CreateWindow(windowClass_.lpszClassName, // クラス名指定
				_T("DX12テスト"),// タイトルバーの文字
				WS_OVERLAPPEDWINDOW, // タイトルバーと境界線があるウィンドウ
				CW_USEDEFAULT,	// 表示X座標
				CW_USEDEFAULT,	// 表示Y座標
				wrc.right - wrc.left, // ウィンドウ幅
				wrc.bottom - wrc.top, // ウィンドウ高
				nullptr, // 親ウィンドウハンドル
				nullptr, // メニューハンドル
				windowClass_.hInstance, // 呼び出しアプリケーションハンドル
				nullptr); // 追加パラメータ

	// 失敗したときに通る
	if (hwnd_ == nullptr)
	{
		LPVOID messageBuffer = nullptr;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			nullptr,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPWSTR)&messageBuffer,
			0,
			nullptr);
		OutputDebugString((TCHAR*)messageBuffer);
		cout << (TCHAR*)messageBuffer << endl;
		LocalFree(messageBuffer);
	}

	dx_ = make_unique<Dx12Wrapper>(hwnd_);
	dx_->Initialize();
}

void Application::Run()
{
	ShowWindow(hwnd_, SW_SHOW);

	MSG msg = {};

	while (true)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) // OSからのメッセージをmsgに格納
		{
			TranslateMessage(&msg); // 仮想キー関連の変換
			DispatchMessage(&msg); // 処理されなかったメッセージをOSに返す
		}

		if (msg.message == WM_QUIT) //もうアプリケーションが終わるって時にWM_QUITにな る
		{
				break;
		}

		if (!dx_->Update())
		{
			break;
		}
	}
}

void Application::Terminate()
{
	dx_->Terminate();
	UnregisterClass(windowClass_.lpszClassName, windowClass_.hInstance);	// 使用しないため登録解除
}

Size Application::GetWindowSize()const
{
	return { WINDOW_WIDTH, WINDOW_HEIGHT };
}

LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	if (msg == WM_DESTROY) // ウィンドウが破壊される
	{
		PostQuitMessage(0); // OSに対して、アプリが終わることを伝える。
		return 0;

	}
	return DefWindowProc(hwnd, msg, wparam, lparam);	//規定の処理を行う
}

Application::~Application() = default;

Application::Application() = default;