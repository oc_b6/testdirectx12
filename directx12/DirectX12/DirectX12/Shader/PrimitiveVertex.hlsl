#include "common.hlsli"

cbuffer SceneData : register(b0)
{
	matrix world; // ワールド行列
	matrix view; 
	matrix prj; // プロジェクション行列
	float3 eye; // 視点
	matrix shadow;
	matrix lightViewProj;
}

Out PrimVS(float4 pos : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD)
{
	Out o;
	o.pos = mul(world, pos);
	o.svpos = mul(mul(prj, view), o.pos);
	o.lvPos = mul(lightViewProj, o.pos);
	o.uv = uv;
	return o;
}