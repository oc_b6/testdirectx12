#include "BoardCommon.hlsli"

Texture2D<float4> tex:register(t0);
SamplerState smp : register(s0);
Texture2D<float4> distTex : register(t1);
Texture2D<float> lightTex : register(t2);

float4 BoardPS(BoardOutput board) : SV_TARGET
{
	if (board.uv.x <= 0.3 && board.uv.y <= 0.3)
	{
		float shadow = lightTex.Sample(smp, board.uv * 3.0f);
		shadow = pow(shadow, 0.25f);
		return float4(shadow, shadow, shadow, 1);
	}
	float4 col = tex.Sample(smp, board.uv);

	//return float4(1 - col.rgb,col.a);
	float2 nmTex = distTex.Sample(smp, board.uv).xy;
	nmTex = nmTex * 2.0f - 1.0f;

	return tex.Sample(smp, board.uv /*+ nmTex * 0.1f*/);
}