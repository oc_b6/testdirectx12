cbuffer Mat:register(b0)
{
	matrix world; // ワールド行列
	matrix view; // プロジェクション行列
	matrix prj; // プロジェクション行列
	float3 eye; // 視点
	matrix shadow;
	matrix lightViewProj;
}

cbuffer Bones:register(b1)
{
	matrix boneMats[512];
}

float4 ShadowVS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD, min16uint2 boneno : BONENO, min16uint weight : WEIGHT, uint instNo : SV_InstanceID):SV_Position
{
	float w = weight / 100.0f;
	matrix m = boneMats[boneno.x] * w + boneMats[boneno.y] * (1 - w);
	matrix mat = mul(world, m);
	mat = mul(lightViewProj, mat);
	pos = mul(mat, pos);
	return pos;
}