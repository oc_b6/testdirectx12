#include "common.hlsli"

Texture2D<float4> tex : register(t0);
Texture2D<float4> sph : register(t1);
Texture2D<float4> spa : register(t2);
Texture2D<float4> toon: register(t3);
Texture2D<float> lightTex : register(t4);
SamplerState smp : register(s0);
SamplerState smpToon : register(s1);


cbuffer Material:register(b1)
{
	float4 diffuse;
	float4 specular;
	float4 ambient;
}


float4 BasicPS(Out o) : SV_TARGET
{
	float3 light = normalize(float3(1,-1,1));	// 光ベクトル

	// diffuse計算
	float diffuseB = saturate( dot(-light, o.normal) );

	// 光の反射ベクトル
	float3 refLight = normalize(reflect(light,o.normal.xyz));
	float specularB = pow(saturate(dot(refLight, -o.ray)), specular.a);

	// sphereMap用UV
	float2 sphereMapUV = o.vnormal.xy;
	sphereMapUV = (sphereMapUV + float2(1, -1)) * float2(0.5, -0.5);

	// テクスチャカラー
	float4 texColor = tex.Sample(smp, o.uv);

	// toon
	float4 toonDif = toon.Sample(smpToon,float2(0,1.0 - diffuseB));	
	
	float ld = o.lvPos.z;
	float2 uv = (float2(1, -1) + o.lvPos.xy) * float2(0.5, -0.5);
	
	float shadowRate = 1.0f;
	
	if (ld > lightTex.Sample(smp, uv) + 0.0005f)
	{
		shadowRate = 0.2f;
	}
	
	float edge = pow(1.0 - abs(dot(o.ray, o.normal.rgb)),1);
	
	//return float4(edge, edge, edge, 1);
	//return float4(float3(edge, edge, edge));
	
	return (saturate(toonDif * diffuse * texColor * sph.Sample(smp, sphereMapUV) * shadowRate
					+ saturate(spa.Sample(smp, sphereMapUV) * texColor + float4(specularB * specular.rgb, 1)
					+ float4(texColor.rgb * ambient * 0.1, 1))) + float4(edge * 0.1, edge * 0.1, edge, 1));
}