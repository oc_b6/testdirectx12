#include "common.hlsli"

Texture2D<float> lightdepthTex : register(t0);
SamplerState smp : register(s0);


float4 PrimPS(Out o) : SV_TARGET
{
	float ld = o.lvPos.z;
	float2 uv = (float2(1, -1) + o.lvPos.xy) * float2(0.5, -0.5);
	if (ld > lightdepthTex.Sample(smp, uv))
	{
		return float4(0.0f, 0.0f, 0.0f, 1.0f);
	}
	

	
	return 1;
}