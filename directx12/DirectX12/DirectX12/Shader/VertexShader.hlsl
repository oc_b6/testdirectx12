#include "common.hlsli"

cbuffer SceneData :register (b0)
{
	matrix world;	// ワールド行列
	matrix view;	// プロジェクション行列
	matrix prj;		// プロジェクション行列
	float3 eye;		// 視点
	matrix shadow;
	matrix lightViewProj;
}

cbuffer Bones:register(b2)
{
	matrix boneMats[512];
}

Out BasicVS(float4 pos : POSITION ,float4 normal:NORMAL,float2 uv: TEXCOORD, min16uint2 boneno : BONENO, min16uint weight : WEIGHT,uint instNo:SV_InstanceID)
{
	Out o;
	float w = weight / 100.0f;
	matrix m = boneMats[boneno.x] * w + boneMats[boneno.y] * (1 - w);
	
	o.pos = mul(m, pos);
	o.pos = mul(world,o.pos);
	o.svpos = mul(mul(prj,view),o.pos);
	o.lvPos = mul(lightViewProj, o.pos);
	normal.w = 0;
	o.normal = mul(world,normal);
	o.vnormal = mul(view,o.normal);
	o.ray = normalize(pos.xyz - eye);
	o.uv = uv;
	o.boneno = boneno;
	o.weight = weight;
	o.instId = instNo;
	return o;
}