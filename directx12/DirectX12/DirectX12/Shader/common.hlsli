struct Out
{
	float4 svpos : SV_POSITION;
	float4 pos : POSITION0;
	float4 lvPos : POSITION1;
	float4 normal : NORMAL0;
	float4 vnormal : NORMAL1;	// ビュー変換後の法線ベクトル
	float2 uv : TEXCOORD;
	float3 ray : VECTOR;
	min16uint2 boneno : BONENO;
	min16uint weight : WEIGHT;
	uint instId : SV_InstanceID;
};

//struct PixelOutput
//{
//	float4 col:
//};