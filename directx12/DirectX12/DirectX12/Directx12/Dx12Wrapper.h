#pragma once
#include <Windows.h>
#include <d3d12.h>
#include <dxgi1_6.h>
#include <vector>
#include <array>
#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <wrl.h>
#include <string>
#include <DirectXTex.h>
#include <memory>
#include <map>
#include <d3dx12.h>
#include <Effekseer.h>
#include <EffekseerRendererDX12.h>

#pragma comment(lib,"DirectXTex.lib")
#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"Effekseerd.lib")
#pragma comment(lib,"EffekseerRendererDX12d.lib")
#pragma comment(lib,"LLGId.lib")

using Microsoft::WRL::ComPtr;

struct Vertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT2 uv;
};

struct SceneData
{
	DirectX::XMMATRIX world;
	DirectX::XMMATRIX view;
	DirectX::XMMATRIX prj;
	DirectX::XMFLOAT4 eye;
	DirectX::XMMATRIX shadow;
	DirectX::XMMATRIX lightViewProj;
};

struct BasicMaterial
{
	DirectX::XMFLOAT3 diffuse;
	float alpha;
	DirectX::XMFLOAT3 specular;
	float specularity;
	DirectX::XMFLOAT3 ambient;
};

struct TextureType 
{
	ComPtr<ID3D12Resource> bmp;
	ComPtr<ID3D12Resource> spa;
	ComPtr<ID3D12Resource> sph;
};

class PMDModel;
class VMDLoader;
struct t_Vertex;
struct BoneNode;
struct Keyframe;
class PrimitiveMesh;
class Dx12Wrapper
{
public:
	Dx12Wrapper();
	Dx12Wrapper(HWND hwnd);
	bool Initialize();
	bool Update();
	void SetBoard();
	void Terminate();
	~Dx12Wrapper();
private:
	// リソースバリア
	bool ResourceBarrier(ComPtr<ID3D12Resource> resource, D3D12_RESOURCE_STATES before, D3D12_RESOURCE_STATES after);

	void ClearRenderTarget();

	// レンダリングがすべて終わるまで待つ
	bool WaitFence();

	// ルートシグネチャ作成
	void InitRootSignature();

	void CreatePipelineState();

	// 頂点バッファを生成(CPU側の頂点情報をコピー)
	void CreateVertexBuffer();

	void InitViewPort();

	void InitScissorRect();

	// インデックスバッファ作成
	void CreateIndexArray();

	void CreateConstantBufferView();

	// 定数バッファ
	void ConstantBuffer();

	// 深度バッファ
	void CreateDepthBufferView();

	// マテリアルバッファ
	void CreateMaterialBuffer();

	// テクスチャ
	ComPtr<ID3D12Resource> LoadTextureFromFile(std::string& texPath);
	ComPtr<ID3D12Resource> CreateAnotherTexture(bool whiteTexture);
	bool CreateTexture(std::string& texPath, ComPtr<ID3D12Resource>&);
	// トゥーンのためのグラデーションテクスチャ
	ComPtr<ID3D12Resource> CreateGrayGradationTexture();

	std::wstring GetWideStringFromString(const std::string& str);
	std::string GetTexturePathFromModelAndTexPath(const std::string& modelPath, const char* texpath);

	// ファイル名から拡張子を取得する
	std::string GetExtension(const std::string& path);

	// ファイル名を分割する
	std::pair<std::string, std::string> SplitFileName(const std::string& path, const char splitter = '*');

	unsigned int AlignedValue(unsigned int value, unsigned int aligne);

	std::array<CD3DX12_STATIC_SAMPLER_DESC, 2> SettingSampler();

	// ボーンバッファ
	void CreateBoneBuffer();

	// ボーン
	void AnimationUpdate(int frameno);
	
	void RotateBone(const char* bonename, const DirectX::XMFLOAT4& q, const DirectX::XMFLOAT4& q2, const float framePer);
	
	void MoveBone(const char* bonename, DirectX::XMFLOAT3 pos1, DirectX::XMFLOAT3 pos2, const float framePer);

	void RecursiveMatrixMultiply(BoneNode& node,DirectX::XMMATRIX& inMat);

	float GetYFromXOnBezierHalfSolve(float x,const DirectX::XMFLOAT2& a,const DirectX::XMFLOAT2& b,uint8_t n = 8);

	void CreateRendertargetTexture();

	void CreateBoardPolyVerts();

	void InitBoardRootSignature();

	void CreateBoardPipeline();

	void SetAndDrawBackBuffer();

	bool CreateEffectBufferAndView();

	bool CreateShadow();

	// 影ルートシグネチャ作成
	void InitShadowRootSignature();

	// 影バッファ作成
	void CreateShadowBufferAndView();

	// 影パイプライン作成
	void CreateShadowPipeline();

	// 影描画
	void DrawShadow();

	// 影レンダーターゲットセット
	void ClearShadow();

	// 影シザーサイズ設定
	void ShadowScissor();

	// 影ビューポートサイズ設定
	void ShadowViewPort();

	// Effekseer初期化
	void InitEffekseer();

	void UpdateEffect();

	ComPtr<ID3D12DescriptorHeap> distortionSRVHeap_ = nullptr;
	ComPtr<ID3D12Resource> distortionTexBuffer_ = nullptr;

	std::array<ComPtr<ID3D12Resource>,2> rtTexture_;
	ComPtr<ID3D12DescriptorHeap> rtvHeapTex_ = nullptr;
	ComPtr<ID3D12DescriptorHeap> srvHeapTex_ = nullptr;
	
	ComPtr<ID3D12Resource> boardPolyVerts_ = nullptr;	// 板ポリ頂点
	ComPtr<ID3D12PipelineState> boardPipeline_ = nullptr;
	ComPtr<ID3D12RootSignature> boardRootSig_ = nullptr;
	D3D12_VERTEX_BUFFER_VIEW boardVBV_;

	ComPtr<ID3D12Resource> depthBuffer_ = nullptr;		// 深度バッファ
	ComPtr<ID3D12DescriptorHeap> depthHeap_ = nullptr;

	ComPtr<ID3D12Resource> shadowDepthBuffer_ = nullptr;	// シャドウマップ用深度バッファ
	ComPtr<ID3D12DescriptorHeap> shadowDSVHeap_ = nullptr;
	ComPtr<ID3D12DescriptorHeap> shadowSRVHeap_ = nullptr;	// 影シェーダリソースビュー
	ComPtr<ID3D12PipelineState> shadowPipeline_ = nullptr;	// 影パイプライン
	ComPtr<ID3D12RootSignature> shadowRootSignature_ = nullptr;	// 影ルートシグネチャ
	D3D12_VIEWPORT shadowViewPort_;	// 影ビューポート
	D3D12_RECT shadowScissor_;

	ComPtr<ID3D12Resource> materialBuffer_;		// マテリアルバッファ
	ComPtr<ID3D12DescriptorHeap> materialHeap_;
	std::vector<std::string> texPath_;
	std::vector<std::map<std::string, ComPtr<ID3D12Resource>>> texResources_;

	std::map<std::string, ComPtr<ID3D12Resource>> resourceTbl_;
	std::vector<ComPtr<ID3D12Resource>> toonResources_;

	ComPtr<ID3D12Device> dev_ = nullptr;
	ComPtr <ID3D12CommandAllocator> cmdAllocator_ = nullptr;
	ComPtr<ID3D12GraphicsCommandList> cmdList_ = nullptr;
	ComPtr<ID3D12CommandQueue> cmdQue_ = nullptr;
	ComPtr<IDXGIFactory6> dxgi_ = nullptr;
	ComPtr<IDXGISwapChain4> swapchain_ = nullptr;
	ComPtr<ID3D12DescriptorHeap> rtvHeap_ = nullptr;
	std::vector<ComPtr<ID3D12Resource>> bbResource;
	HWND hwnd_;

	ComPtr <ID3D12Fence> fence_ = nullptr;
	UINT64 fenceValue_ = 0;

	std::vector<t_Vertex> vertices_;		// 頂点データ
	ComPtr<ID3D12Resource> vertexBuffer_ = nullptr;
	D3D12_VERTEX_BUFFER_VIEW vbView_;

	ComPtr<ID3D12PipelineState> pipeline_ = nullptr;		// パイプライン
	ComPtr<ID3D12RootSignature> rootSignature_ = nullptr;	// ルートシグネチャ

	ComPtr<ID3DBlob> errorBlob_ = nullptr;	   //エラー出た時の対処

	D3D12_VIEWPORT viewPort_;	// ビューポート
	D3D12_RECT scissorRect_;

	std::vector<unsigned short> indices_;		// 頂点数
	ComPtr<ID3D12Resource> idxBuff_ = nullptr;
	D3D12_INDEX_BUFFER_VIEW ibView_;

	ComPtr<ID3D12DescriptorHeap> transformHeap_ = nullptr;	// その他(テクスチャ、定数)デスクリプタヒープ
	
	ComPtr<ID3D12Resource> cbvBuffer_ = nullptr;

	SceneData* mat_;

	// カメラ情報
	DirectX::XMFLOAT4 eye_;		// カメラの座標
	DirectX::XMFLOAT3 target_;	// 見る場所
	DirectX::XMFLOAT3 up_;		// カメラの上ベクトル

	std::shared_ptr<PMDModel> pmdModel_;

	std::unique_ptr<VMDLoader> vmdLoader_;

	std::map<std::string, BoneNode> boneMap_;
	std::vector<DirectX::XMMATRIX> boneMatrix_;		// ボーン行列情報
	ComPtr<ID3D12Resource> boneBuffer_;				// ボーンバッファ
	ComPtr<ID3D12DescriptorHeap> boneHeap_;			// ボーンバッファ
	
	DirectX::XMMATRIX* mappedBonesMatrix_;

	int maxFrame_ = 0;

	std::map<std::string, std::vector<Keyframe>> animDataMap_;

	std::shared_ptr<PrimitiveMesh> planeMesh_;

	// キーボード
	BYTE keyState[256];
	BYTE lastKeyState[256];

	// effekseer
	EffekseerRenderer::Renderer* efkRenderer_;
	Effekseer::Manager* efkManager_;
	EffekseerRenderer::SingleFrameMemoryPool* efkMemoryPool_;
	EffekseerRenderer::CommandList* efkCmdList_;
	Effekseer::Effect* effect_;
	Effekseer::Handle effectHandle_ = 0;

	std::array<ComPtr<ID3D12Resource>, 2> board1Resources_;
};
