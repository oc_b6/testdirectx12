#include <assert.h>
#include <d3dx12.h>
#include <tchar.h>
#include <d3dcompiler.h>
#include "PrimitiveMesh.h"
#include "Application.h"

using namespace DirectX;

PrimitiveMesh::~PrimitiveMesh()
{

}

void PrimitiveMesh::InitPrimRootSignature()
{
	D3D12_DESCRIPTOR_RANGE range[2] = {};
	range[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	range[0].BaseShaderRegister = 0;
	range[0].NumDescriptors = 1;
	range[0].RegisterSpace = 0;
	range[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	range[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	range[1].BaseShaderRegister = 0;
	range[1].NumDescriptors = 1;
	range[1].RegisterSpace = 0;
	range[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;


	D3D12_ROOT_PARAMETER rootparam[2] = {};
	rootparam[0].DescriptorTable.NumDescriptorRanges = 1;
	rootparam[0].DescriptorTable.pDescriptorRanges = &range[0];
	rootparam[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootparam[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;

	rootparam[1].DescriptorTable.NumDescriptorRanges = 1;
	rootparam[1].DescriptorTable.pDescriptorRanges = &range[1];
	rootparam[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootparam[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	CD3DX12_STATIC_SAMPLER_DESC samplerDesc = {};
	samplerDesc.Init(0, D3D12_FILTER_MIN_MAG_MIP_POINT);
	samplerDesc.BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK; // エッジの色(黒透明)
	samplerDesc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;	// 常に通る

	D3D12_ROOT_SIGNATURE_DESC rootSigDesc = {};
	rootSigDesc.pParameters = rootparam;
	rootSigDesc.NumParameters = _countof(rootparam);
	rootSigDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
	rootSigDesc.pStaticSamplers = &samplerDesc;
	rootSigDesc.NumStaticSamplers = 1;

	ComPtr<ID3DBlob> primSigBlob = nullptr;
	ComPtr<ID3DBlob> primErrorBlob = nullptr;

	auto result = D3D12SerializeRootSignature(&rootSigDesc,
		D3D_ROOT_SIGNATURE_VERSION_1_0,
		&primSigBlob,
		&primErrorBlob);
	assert(SUCCEEDED(result));

	result = dev_->CreateRootSignature(0,
		primSigBlob->GetBufferPointer(),
		primSigBlob->GetBufferSize(),
		IID_PPV_ARGS(primRootSignature_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));
}

void PrimitiveMesh::CreatePrimBufferAndView()
{
	vertices_.emplace_back(XMFLOAT3(pos_.x - width_ / 2, pos_.y, pos_.z - depth_ / 2), XMFLOAT3(0, 1, 0), XMFLOAT2(0, 0));
	vertices_.emplace_back(XMFLOAT3(pos_.x - width_ / 2, pos_.y, pos_.z + depth_ / 2), XMFLOAT3(0, 1, 0), XMFLOAT2(0, 1));
	vertices_.emplace_back(XMFLOAT3(pos_.x + width_ / 2, pos_.y, pos_.z - depth_ / 2), XMFLOAT3(0, 1, 0), XMFLOAT2(1, 0));
	vertices_.emplace_back(XMFLOAT3(pos_.x + width_ / 2, pos_.y, pos_.z + depth_ / 2), XMFLOAT3(0, 1, 0), XMFLOAT2(1, 1));

	HRESULT result = S_OK;
	result = dev_->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(PrimitiveData) * vertices_.size()),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(vertexBuffer_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));

	PrimitiveData* verMap = nullptr;
	result = vertexBuffer_->Map(0, nullptr, (void**)&verMap);
	assert(SUCCEEDED(result));
	std::copy(vertices_.begin(), vertices_.end(), verMap);
	vertexBuffer_->Unmap(0, nullptr);

	vbView_.BufferLocation = vertexBuffer_->GetGPUVirtualAddress();
	vbView_.SizeInBytes = sizeof(PrimitiveData) * vertices_.size();
	vbView_.StrideInBytes = sizeof(vertices_[0]);
}

void PrimitiveMesh::CreatePrimPipeline()
{
	ComPtr<ID3DBlob> vertexShader = nullptr;// 頂点シェーダ
	ComPtr<ID3DBlob> pixelShader = nullptr;	// ピクセルシェーダ
	ComPtr<ID3DBlob> errorBlob = nullptr;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC plsDesc = {};

	D3D12_INPUT_ELEMENT_DESC layout[] = { {"POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										  {"NORMAL",0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										{"TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0}};

	plsDesc.InputLayout.NumElements = _countof(layout);
	plsDesc.InputLayout.pInputElementDescs = layout;
	plsDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

	// 頂点シェーダ
	auto result = D3DCompileFromFile(
		_T("Shader/PrimitiveVertex.hlsl"), // シェーダファイルパス
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"PrimVS",  // エントリポイント名
		"vs_5_1",	// シェーダバージョン
		D3DCOMPILE_DEBUG |
		D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&vertexShader,
		&errorBlob);
	assert(SUCCEEDED(result));

	if (errorBlob != nullptr)
	{
		std::string errstr = "";
		auto errSize = errorBlob->GetBufferSize();
		errstr.resize(errSize);
		std::copy_n(static_cast<char*>(errorBlob->GetBufferPointer()), errorBlob->GetBufferSize(), errstr.begin());
	}
	plsDesc.VS.BytecodeLength = vertexShader->GetBufferSize();
	plsDesc.VS.pShaderBytecode = vertexShader->GetBufferPointer();

	// ピクセルシェーダ
	result = D3DCompileFromFile(
		_T("Shader/PrimitivePixel.hlsl"),
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"PrimPS",
		"ps_5_1",
		D3DCOMPILE_DEBUG |
		D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&pixelShader,
		&errorBlob);
	assert(SUCCEEDED(result));

	if (errorBlob != nullptr)
	{
		std::string errstr = "";
		auto errSize = errorBlob->GetBufferSize();
		errstr.resize(errSize);
		std::copy_n(static_cast<char*>(errorBlob->GetBufferPointer()), errorBlob->GetBufferSize(), errstr.begin());
	}
	plsDesc.PS.BytecodeLength = pixelShader->GetBufferSize();
	plsDesc.PS.pShaderBytecode = pixelShader->GetBufferPointer();

	// ラスタライザ設定
	plsDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	plsDesc.RasterizerState.FillMode = D3D12_FILL_MODE_SOLID;
	plsDesc.RasterizerState.DepthClipEnable = true;

	// 深度・ステンシル設定
	plsDesc.DepthStencilState.DepthEnable = true;
	plsDesc.DepthStencilState.StencilEnable = false;
	plsDesc.NodeMask = 0;
	plsDesc.SampleDesc.Count = 1;
	plsDesc.SampleDesc.Quality = 0;
	plsDesc.SampleMask = 0xffffffff;
	plsDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	plsDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	plsDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;

	// 出力設定
	plsDesc.BlendState.AlphaToCoverageEnable = false;
	plsDesc.BlendState.IndependentBlendEnable = false;
	plsDesc.BlendState.RenderTarget[0].BlendEnable = false;
	plsDesc.BlendState.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	plsDesc.pRootSignature = primRootSignature_.Get();
	plsDesc.NumRenderTargets = 1;
	plsDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	result = dev_->CreateGraphicsPipelineState(&plsDesc, IID_PPV_ARGS(primPipeline_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));
}

void PrimitiveMesh::DrawPrim(ComPtr<ID3D12DescriptorHeap>& transformHeap, ComPtr<ID3D12DescriptorHeap>& shadowSRVHeap)
{
	cmdList_->SetPipelineState(primPipeline_.Get());
	cmdList_->SetGraphicsRootSignature(primRootSignature_.Get());
	cmdList_->SetDescriptorHeaps(1, transformHeap.GetAddressOf());
	cmdList_->SetGraphicsRootDescriptorTable(0, transformHeap->GetGPUDescriptorHandleForHeapStart());
	cmdList_->SetDescriptorHeaps(1, shadowSRVHeap.GetAddressOf());
	cmdList_->SetGraphicsRootDescriptorTable(1, shadowSRVHeap->GetGPUDescriptorHandleForHeapStart());
	cmdList_->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	cmdList_->IASetVertexBuffers(0, 1, &vbView_);
	cmdList_->DrawInstanced(4, 1, 0, 0);
}
