#include <stdio.h>

#include "VMDLoader.h"

using namespace DirectX;

VMDLoader::VMDLoader()
{
}

bool VMDLoader::LoadFile(const char* filePath)
{
	DWORD keyframeNum;
	FILE* file;
	fopen_s(&file, filePath, "rb");
	
	if (file == nullptr)
	{
		return false;
	}
	// ヘッダ
	fseek(file,50,SEEK_SET);
	// モーションデータ数
	fread(&keyframeNum,sizeof(DWORD), 1, file);
	// モーションデータ
	keyframes.resize(keyframeNum);
	for (auto& keyframe : keyframes)
	{
		fread(keyframe.boneName,sizeof(keyframe.boneName),1,file);	// ボーン名
		fread(&keyframe.frameNo, 
			sizeof(keyframe.frameNo) // フレーム番号
			+ sizeof(keyframe.location) // 位置
			+ sizeof(keyframe.quaternion) // 回転
			+ sizeof(keyframe.interpolation), // 補間ベジエデータ
			1, file);
	}

	fclose(file);

	for (auto& f : keyframes)
	{
		animData_[f.boneName].emplace_back(Keyframe(f.frameNo,f.quaternion,f.location,
			XMFLOAT2((float)f.interpolation[3 + 15] / 127.0f,(float)f.interpolation[7 + 15] / 127.0f),
			XMFLOAT2((float)f.interpolation[11 + 15] / 127.0f, (float)f.interpolation[15 + 15] / 127.0f)));
	}

	return true;
}

const std::map<std::string, std::vector<Keyframe>> VMDLoader::GetAnimationData()
{
	return animData_;
}
