#include "PMDRenderer.h"
#include <tchar.h>

PMDRenderer::PMDRenderer(Dx12Wrapper* dx12)
{
	dx12_ = dx12;
	InitRootSignature();
	SettingSampler();
}

PMDRenderer::~PMDRenderer()
{
}

void PMDRenderer::InitRootSignature()
{
	HRESULT result = S_OK;
	// ルートシグネチャ
	D3D12_ROOT_SIGNATURE_DESC rootSigDesc = {};
	CD3DX12_ROOT_PARAMETER rootParam[2] = {};
	CD3DX12_DESCRIPTOR_RANGE descRange[3] = {};
	std::array<CD3DX12_STATIC_SAMPLER_DESC, 2> samplerDesc = {};

	samplerDesc = SettingSampler();

	// 座標
	// 引数:rangeType,numDescriptors,baseShaderRagister
	descRange[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	// マテリアル
	descRange[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 1);
	// テクスチャ
	descRange[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 4, 0);

	// ルートパラメータ
	rootParam[0].InitAsDescriptorTable(1, &descRange[0]);
	rootParam[1].InitAsDescriptorTable(2, &descRange[1]);
	rootParam[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootSigDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
	rootSigDesc.pParameters = rootParam;
	rootSigDesc.NumParameters = 2;
	rootSigDesc.pStaticSamplers = &samplerDesc[0];
	rootSigDesc.NumStaticSamplers = 2;
	result = D3D12SerializeRootSignature(&rootSigDesc,
		D3D_ROOT_SIGNATURE_VERSION_1_0,
		&signatureBlob_,
		&errorBlob_);
	assert(SUCCEEDED(result));

	result = dev_->CreateRootSignature(0,
		signatureBlob_->GetBufferPointer(),
		signatureBlob_->GetBufferSize(),
		IID_PPV_ARGS(&rootSignature_));
	assert(SUCCEEDED(result));
}

std::array<CD3DX12_STATIC_SAMPLER_DESC, 2> PMDRenderer::SettingSampler()
{
	std::array<CD3DX12_STATIC_SAMPLER_DESC, 2> samplerDesc = {};
	samplerDesc[0].Init(0, D3D12_FILTER_MIN_MAG_MIP_POINT);
	samplerDesc[0].BorderColor = D3D12_STATIC_BORDER_COLOR_TRANSPARENT_BLACK; // エッジの色(黒透明)
	samplerDesc[0].ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;	// 常に通る
	samplerDesc[1].Init(1, D3D12_FILTER_ANISOTROPIC, D3D12_TEXTURE_ADDRESS_MODE_CLAMP,
		D3D12_TEXTURE_ADDRESS_MODE_CLAMP, D3D12_TEXTURE_ADDRESS_MODE_CLAMP);
	return samplerDesc;
}

const ComPtr<ID3D12PipelineState> PMDRenderer::GetPipelineState()
{
	return pipeline_;
}

const ComPtr<ID3D12RootSignature> PMDRenderer::GetRootSignature()
{
	return rootSignature_;
}

void PMDRenderer::CreatePipelineState()
{
	HRESULT result = S_OK;
	D3D12_GRAPHICS_PIPELINE_STATE_DESC plsDesc = {};
	D3D12_INPUT_ELEMENT_DESC layout[] = { {"POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										 {"NORMAL",0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0},
										 {"TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,D3D12_APPEND_ALIGNED_ELEMENT,D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,0} };

	plsDesc.InputLayout.NumElements = _countof(layout);
	plsDesc.InputLayout.pInputElementDescs = layout;
	plsDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

	// 頂点シェーダ
	result = D3DCompileFromFile(
		_T("Shader/VertexShader.hlsl"), // シェーダファイルパス
		nullptr, // defineマクロオブジェクト
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // includeオブジェクト
		"BasicVS",  // エントリポイント名
		"vs_5_1",	// シェーダバージョン
		D3DCOMPILE_DEBUG |
		D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&vertexShader_,
		&errorBlob_);
	assert(SUCCEEDED(result));

	if (errorBlob_ != nullptr)
	{
		std::string errstr = "";
		auto errSize = errorBlob_->GetBufferSize();
		errstr.resize(errSize);
		std::copy_n(static_cast<char*>(errorBlob_->GetBufferPointer()), errorBlob_->GetBufferSize(), errstr.begin());
	}
	plsDesc.VS.BytecodeLength = vertexShader_->GetBufferSize();
	plsDesc.VS.pShaderBytecode = vertexShader_->GetBufferPointer();

	// ピクセルシェーダ
	result = D3DCompileFromFile(
		_T("Shader/PixelShader.hlsl"),
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"BasicPS",
		"ps_5_1",
		D3DCOMPILE_DEBUG |
		D3DCOMPILE_SKIP_OPTIMIZATION,
		0,
		&pixelShader_,
		&errorBlob_);
	assert(SUCCEEDED(result));

	if (errorBlob_ != nullptr)
	{
		std::string errstr = "";
		auto errSize = errorBlob_->GetBufferSize();
		errstr.resize(errSize);
		std::copy_n(static_cast<char*>(errorBlob_->GetBufferPointer()), errorBlob_->GetBufferSize(), errstr.begin());
	}

	plsDesc.PS.BytecodeLength = pixelShader_->GetBufferSize();
	plsDesc.PS.pShaderBytecode = pixelShader_->GetBufferPointer();

	// ラスタライザ設定
	plsDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	plsDesc.RasterizerState.FillMode = D3D12_FILL_MODE_SOLID;
	plsDesc.RasterizerState.DepthClipEnable = true;

	// 深度・ステンシル設定
	plsDesc.DepthStencilState.DepthEnable = true;
	plsDesc.DepthStencilState.StencilEnable = false;
	plsDesc.NodeMask = 0;
	plsDesc.SampleDesc.Count = 1;
	plsDesc.SampleDesc.Quality = 0;
	plsDesc.SampleMask = 0xffffffff;
	plsDesc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	plsDesc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	plsDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;

	// 出力設定
	plsDesc.BlendState.AlphaToCoverageEnable = false;
	plsDesc.BlendState.IndependentBlendEnable = false;
	plsDesc.BlendState.RenderTarget[0].BlendEnable = false;
	plsDesc.BlendState.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	plsDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
	plsDesc.pRootSignature = rootSignature_.Get();
	plsDesc.NumRenderTargets = 1;

	result = dev_->CreateGraphicsPipelineState(&plsDesc, IID_PPV_ARGS(pipeline_.ReleaseAndGetAddressOf()));
	assert(SUCCEEDED(result));
}
