#pragma once
#include <Windows.h>
#include <memory>

struct Size
{
	int w;
	int h;
};

class Dx12Wrapper;
class Application
{
public:

	static Application& Instance()
	{
		static Application s_instance;
		return s_instance;
	}

	void Initialize();
	void Run();
	void Terminate();

	Size GetWindowSize()const;

	~Application();
private:
	Application();
	Application(const Application&) = delete;
	void operator=(const Application&) = delete;
	WNDCLASSEX windowClass_; 
	HWND hwnd_;	// ウィンドウハンドル
	std::unique_ptr<Dx12Wrapper> dx_;
};

