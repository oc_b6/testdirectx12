#pragma once
#include <Windows.h>
#include <DirectXMath.h>
#include <vector>
#include <string>

#pragma pack(1)
struct PMDHeader
{
	float version;
	char model_name[20];
	char comment[256];
};

struct t_Vertex
{
	DirectX::XMFLOAT3 pos;	// 座標
	DirectX::XMFLOAT3 normal_vec;  // 法線ベクトル
	DirectX::XMFLOAT2 uv;		// UV座標 
	WORD bone_num[2];	 // ボーン番号1、番号2  モデル変形(頂点移動)時に影響
	BYTE bone_weight;	 // ボーン1に与える影響度 // min:0 max:100 // ボーン2への影響度は、(100 - bone_weight)
	BYTE edge_flag;	// エッジ(輪郭)が有効の場合
};

struct Material
{
	DirectX::XMFLOAT3 diffuse; // 減衰色
	float alpha; // 減衰色の不透明度
	float specularity;
	DirectX::XMFLOAT3 specular;  // 光沢色
	DirectX::XMFLOAT3 ambient;  // 環境色(ambient)
	BYTE toon_index; 
	BYTE edge_flag; // 輪郭、影
	unsigned int indicesNum; // 面頂点数
	char texFilePath[20];
};

struct Bone
{
	char bone_name[20]; // ボーン名
	WORD parent_bone_index; // 親ボーン番号(ない場合は0xFFFF)
	WORD tail_pos_bone_index; // 親：子は1：多なので、主に位置決め用
	BYTE bone_type; // ボーンの種類
	WORD ik_parent_bone_index; // IKボーン番号(影響IKボーン。ない場合は0)
	DirectX::XMFLOAT3 bone_head_pos; // ボーンのヘッドの位置
};

struct PMDBone
{
	std::string name;	// ボーンの名前
	std::vector<int> children;	// 子ボーンの番号
	std::vector<std::string> childrenName;	// 子ボーンの番号
	DirectX::XMFLOAT3 pos;
};

struct BoneNode
{
	int boneIdx;
	DirectX::XMFLOAT3 startPos;
	DirectX::XMFLOAT3 endPos;
	std::vector<BoneNode*> children;
};

#pragma pack()

class PMDModel
{
public:
	PMDModel();

	bool LoadFile(const char* filePath);
	const std::vector<t_Vertex>& GetVertexData();
	const std::vector<uint16_t>& GetIndices();
	const std::vector<Material>& GetMaterial();
	const std::vector<std::string>& GetToonPath();
	const std::vector<Bone>& GetBone();
private:
	std::vector<t_Vertex> pmdvertices_;
	std::vector<uint16_t> pmdIndices_;
	std::vector<Material> pmdMaterials_;

	std::vector<std::string> toonPaths_;
	std::vector<PMDBone> bones_;	// ボーン情報
	std::vector<Bone> bone_;
};
